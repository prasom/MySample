﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System;

namespace NorthwindModel
{
    public class InventoryDetail 
    {
        public Int16 UnitsInStock { get; set; }
        public Int16 UnitsOnOrder { get; set; }
        public Int16 ReorderLevel { get; set; }
    }
}
