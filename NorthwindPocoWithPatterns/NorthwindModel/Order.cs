﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;

namespace NorthwindModel
{
    public class Order
    {
        public Order()
        {
            // default constructor for supporting entity materialization
        }

        public Order(Customer customer, int employeeID, int shipVia)
        {
            Customer = customer;
            EmployeeID = employeeID;
            ShipVia = shipVia;
            OrderDate = DateTime.Today;
            RequiredDate = DateTime.Today.AddDays(5);
            ShippedDate = null;
            ShipAddress = customer.Address;
            ShipCity = customer.City;
            ShipRegion = customer.Region;
            ShipCountry = customer.Country;
            ShipPostalCode = customer.PostalCode;

            customer.AddOrder(this);
        }

        public int OrderID { get; set; }
        public int EmployeeID { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int ShipVia { get; set; }
        public decimal Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }
        public Customer Customer { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }

        public void AddOrderDetail(OrderDetail orderDetail)
        {
            if (OrderDetails == null)
            {
                OrderDetails = new List<OrderDetail>();
            }

            if (!OrderDetails.Contains(orderDetail))
            {
                OrderDetails.Add(orderDetail);
            }

            orderDetail.Order = this;
        }

        public void CalculateAndApplyShippingCharges()
        {
            // This is where you might apply logic to calcualte shipping 
            // charges and apply to order
            // and you can implement additional business logic on top of 
            // your entities if you wish to
        }

        public void AddNewOrderDetail(Product product, short quantity)
        {
            AddOrderDetail(new OrderDetail { Order = this, Product = product, Quantity = quantity, UnitPrice = product.UnitPrice });
        }
    }
}
