﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Collections.Generic;

namespace NorthwindModel
{
    // All persist properties are virtual for proxy based change tracking
    public class Product
    {
        public virtual int ProductID { get; set; }
        public virtual string ProductName { get; set; }
        public virtual int SupplierID { get; set; }
        public virtual string QuantityPerUnit { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual InventoryDetail InventoryDetail { get; set; }
        public virtual bool Discontinued { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}