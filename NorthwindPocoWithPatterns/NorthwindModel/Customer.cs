﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Collections.Generic;

namespace NorthwindModel
{
    public class Customer
    {        
        public string CustomerID { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public List<Order> Orders { get; set; }

        public void AddOrder(Order order)
        {
            if (Orders == null)
            {
                Orders = new List<Order>();
            }

            if (!Orders.Contains(order))
            {
                Orders.Add(order);
            }

            order.Customer = this;
        }
    }
}
