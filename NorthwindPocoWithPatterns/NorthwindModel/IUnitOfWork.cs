﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
namespace NorthwindModel
{
    public interface IUnitOfWork
    {        
        void Save();
    }
}
