﻿namespace NorthwindModel.Repositories
{
    public interface IOrderRepository
    {
        void AddOrder(Order order);
    }
}
