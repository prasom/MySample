﻿namespace NorthwindModel.Repositories
{
    public interface IProductRepository
    {
        Product GetProductById(int id);
    }
}
