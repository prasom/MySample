﻿using System;
using System.Collections.Generic;

namespace NorthwindModel.Repositories
{
    public interface ICustomerRepository
    {        
        Customer GetCustomerById(string id);
        IEnumerable<Customer> FindByName(string name);
        void AddCustomer(Customer customer);
    }
}
