﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Collections.Generic;

namespace NorthwindModel
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }

        // Products is declared as virtual so that we get automatic Deferred loading
        public virtual List<Product> Products { get; set; }
        
        public void AddProduct(Product p)
        {
            if (Products == null)
            {
                Products = new List<Product>();
            }

            if (!Products.Contains(p))
            {
                Products.Add(p);
            }

            p.Category = this;
        }        
    }
}
