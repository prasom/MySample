﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NorthwindData;
using NorthwindData.Repositories;
using NorthwindModel;

namespace NorthwindPocoTest
{
    [TestClass]
    public class OrderRepositoryTests
    {
        private NorthwindContext context;

        [TestInitialize]
        public void Start()
        {
            context = new NorthwindContext();
        }

        [TestCleanup]
        public void Finish()
        {
            context.Dispose();
        }

        /// <summary>
        /// This example shows how you can use multiple repositories that are enlisted into the
        /// same unit of work in order to group related queries and updates
        /// </summary>
        [TestMethod]
        public void AddOrderSavesToDatabase()
        {            
            CustomerRepository customerRepository = new CustomerRepository(context as IUnitOfWork);
            OrderRepository orderRepository = new OrderRepository(context as IUnitOfWork);
            ProductRepository productRepository = new ProductRepository(context as IUnitOfWork);

            Customer customer = customerRepository.GetCustomerById("ALFKI");
            Product product = productRepository.GetProductById(1);

            Order order = new Order(customer, 1, 1); // Arguments are customer, employeeId, ShipVia
            order.AddNewOrderDetail(product, 1); // Arguments are product, quantity     
            
            order.CalculateAndApplyShippingCharges();
            
            orderRepository.AddOrder(order);

            // Before saving the order, we expect OrderID to be 0
            // because OrderID is assigned during save
            Assert.AreEqual(0, order.OrderID);

            context.Save();

            // OrderID should be assigned a non-zero unique value
            // after a successful save
            Assert.AreNotEqual(0, order.OrderID);

        }
    }
}
