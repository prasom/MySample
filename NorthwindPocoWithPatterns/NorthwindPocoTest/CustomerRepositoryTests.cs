﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NorthwindData;
using NorthwindData.Repositories;
using NorthwindModel;

namespace NorthwindPocoTest
{
    [TestClass]
    public class CustomerRepositoryTests
    {
        private NorthwindContext context;

        [TestInitialize]
        public void Start()
        {
            context = new NorthwindContext();
        }

        [TestCleanup]
        public void Finish()
        {
            context.Dispose();
        }

        /// <summary>
        /// Querying by a valid ID should return rows from the database
        /// </summary>
        [TestMethod]
        public void GetByValidIdReturnsARow()
        {            
            CustomerRepository repository = new CustomerRepository(context);
            Customer customer = repository.GetCustomerById("ALFKI");

            Assert.IsNotNull(customer);
            Assert.AreEqual("Maria Anders", customer.ContactName);
        }

        /// <summary>
        /// Find by name should return rows from the database
        /// </summary>
        [TestMethod]
        public void FindByNameReturnsRows()
        {
            CustomerRepository repository = new CustomerRepository(context);
            var customers = repository.FindByName("M");

            Assert.IsNotNull(customers);
            Assert.AreEqual(12, customers.Count());
        }
    }
}
