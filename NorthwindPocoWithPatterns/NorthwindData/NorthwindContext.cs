﻿//Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Data.Objects;
using NorthwindModel;

namespace NorthwindData
{
    public class NorthwindContext : ObjectContext, IUnitOfWork
    {   
        public NorthwindContext() : base("name=NorthwindEntities", "NorthwindEntities")
        {
            _categories = CreateObjectSet<Category>();
            _products = CreateObjectSet<Product>();
            _customers = CreateObjectSet<Customer>();
            _orders = CreateObjectSet<Order>();
        }

        public ObjectSet<Category> Categories
        {
            get 
            { 
                return _categories;
            }
        }
        private ObjectSet<Category> _categories;

        public ObjectSet<Product> Products
        {
            get
            {
                return _products;
            }
        }
        private ObjectSet<Product> _products;

        public ObjectSet<Customer> Customers
        {
            get
            {
                return _customers;
            }
        }
        private ObjectSet<Customer> _customers;

        public ObjectSet<Order> Orders
        {
            get
            {
                return _orders;
            }
        }
        private ObjectSet<Order> _orders;

        public void Save()
        {
            SaveChanges();
        }        
    }
}
