﻿using System;
using System.Collections.Generic;
using System.Linq;
using NorthwindModel;
using NorthwindModel.Repositories;

namespace NorthwindData.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private NorthwindContext _context;

        public CustomerRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _context = unitOfWork as NorthwindContext;
        }

        public Customer GetCustomerById(string id)
        {
            return _context.Customers.Where(c => c.CustomerID == id).Single();
        }

        public IEnumerable<Customer> FindByName(string name)
        {
            return _context.Customers.Where(c => c.ContactName.StartsWith(name)).ToList();
        }

        public void AddCustomer(Customer customer)
        {
            _context.Customers.AddObject(customer);
        }
    }
}
