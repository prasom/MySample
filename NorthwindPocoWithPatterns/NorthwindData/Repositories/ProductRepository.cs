﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using NorthwindModel;
using NorthwindModel.Repositories;

namespace NorthwindData.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private NorthwindContext _context;

        public ProductRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _context = unitOfWork as NorthwindContext;
        }

        public Product GetProductById(int id)
        {
            Product product = (from p in _context.Products
                               where p.ProductID == id
                               select p).Single();

            return product;
        }
    }
}