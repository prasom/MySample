﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using NorthwindModel;
using NorthwindModel.Repositories;

namespace NorthwindData.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private NorthwindContext _context; 

        public OrderRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _context = unitOfWork as NorthwindContext;
        }        

        public void AddOrder(Order order)
        {
            _context.Orders.AddObject(order);
        }
    }
}